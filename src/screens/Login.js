import React, { Component } from "react";
import { MenuContainer } from "../components";
import { MessageFeed } from "../components/message-feed/MessageFeed";
import { LoginFormContainer } from "../components/login-form";
import { MessageFeedContainer } from "../components/message-feed";

export const LoginScreen = () => (
  <>
    <MenuContainer />
    <h2>Your favorite microblogging platform</h2>
    <LoginFormContainer />
  </>
);
