import React from "react";
import { MenuContainer } from "../../components";
import { RegisterFormContainer } from "../../components/register-form/index";
export const Register = () => (
  <>
    <MenuContainer />
    <h2>Your favorite microblogging platform</h2>
    <RegisterFormContainer />
  </>
);
