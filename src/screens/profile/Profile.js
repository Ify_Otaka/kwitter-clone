import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { MenuContainer } from "../../components";
import { getProfileAction } from "../../redux/actions/users";
import { getPhotoAction } from "../../redux/actions/users";
import { updatePhotoAction } from "../../redux/actions/users";

import api from "../../utils/api";

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: null,
      picture: null,
      userName: null,
    };
  }

  componentWillMount() {
    this.fetchUser();
  }

  fetchUser = () => {
    let userName = this.props.match.params.username;
    this.setState({ userName });
    return api.getUser(userName).then((result) => {
      this.props.dispatch(getProfileAction(result));
    });
  };

  fetchUserPhoto = () => {
    let userName = this.props.match.params.username;
    return api.getPhoto(userName).then((result) => {
      this.props.dispatch(getPhotoAction(result));
    });
  };

  onImageChange = (e) => {
    const file = e.target.files[0];
    this.setState({
      photo: file,
    });
  };

  updatePhoto = (e) => {
    e.preventDefault();

    api
      .setUserPhoto(this.state.photo, this.props.match.params.username)
      .then((result) => {
        this.props.dispatch(updatePhotoAction(result));
      });
  };

  render() {
    return (
      <div>
        <MenuContainer username={this.props.auth.username} />

        {this.props.profile && this.props.profile.user && (
          <h1 id={this.props.profile.user.username}>
            {this.props.profile.user.displayName}
          </h1>
        )}
        <h3>About</h3>
        <p>{this.props.about}</p>
        {this.props.profile &&
        this.props.profile.user &&
        this.props.profile.user.pictureLocation ? (
          <img
            src={`https://kwitter-api.herokuapp.com${this.props.profile.user.pictureLocation}`}
            alt={"Profile pic"}
          />
        ) : (
          <img src={require("./unnamed.png")} alt={""} />
        )}

        <h2>Update photo</h2>

        <form className="form" onSubmit={this.updatePhoto}>
          <input
            type="file"
            name="picture"
            id="picture"
            onChange={this.onImageChange}
          ></input>
          <button type="submit">submit</button>
        </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    profile: state.users.profile,
    about:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Fringilla ut morbi tincidunt augue.",
  };
}
export default connect(mapStateToProps)(Profile);
