import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { LoginFormContainer, MenuContainer } from "../../components";
import { getUsersAction } from "../../redux/actions/users";
import { MessageFeedContainer } from "../../components/message-feed";
import api from "../../utils/api";
// import { getKweetfeed } from "../redux/actions/messages";
// import { getKweet } from "../redux/actions/messages";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: null,
    };
  }

  componentDidMount() {
    this.fetchUsersList();
  }

  fetchUsersList = () => {
    return api.getUsersList().then((result) => {
      this.props.dispatch(getUsersAction(result));
    });
  };

  // fetchKweetfeed = () => {
  //   return api.getMessagesList().then((result) => {
  //     this.props.dispatch(getKweetfeedAction(result));
  //   });
  // };

  // fetchKweet = () => {
  //   return api.getMessage().then((result) => {
  //     this.props.dispatch(getKweetAction(result));
  //   });
  // };

  render() {
    console.log(this.props.users);
    if (this.props.users) {
      return (
        <div>
          <MenuContainer username={this.props.auth.username} />
          {!this.props.auth.isAuthenticated && <LoginFormContainer />}
          {/*<h2>Home</h2> */}
          <div>
            {/* {this.props.auth.isAuthenticated &&
              this.props.users.users.users.map((user) => (
                <Link to={`/profiles/${user.username}/`}>
                  <div id={user.username}>{user.displayName}</div>
                </Link>
              ))} */}
          </div>
        </div>
      );
    }
    return <div className="loading_screen">{"Loading..."}</div>;
  }
}

function mapStateToProps(state) {
  return {
    users: state.users,
    auth: state.auth,
  };
}

export default connect(mapStateToProps)(Home);
