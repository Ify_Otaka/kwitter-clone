import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import api from "../../utils/api";
import {LoginFormContainer,MenuContainer } from "../../components";
import { getUsersAction } from "../../redux/actions/users";




class AllUsers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: null,
    };
  }

  componentDidMount() {
    this.fetchUsersList();
  }

  fetchUsersList = () => {
    return api.getUsersList().then((result) => {
      this.props.dispatch(getUsersAction(result));
    });
  };




   render() {
    console.log(this.props.users);
    if (this.props.users) {
      return (
        <div>
          <MenuContainer />
          {!this.props.auth.isAuthenticated && <LoginFormContainer />}
          <h2>Home</h2>
          <div>
            {this.props.auth.isAuthenticated &&
              this.props.users.users.users.map((user) => (
                <Link to={`/profiles/${user.username}/`}>
                  <div id={user.username}>{user.displayName}</div>
                </Link>
              ))}
          </div>
        </div>
      );
    }
    return <div className="loading_screen">{"Loading..."}</div>;
  }
}

function mapStateToProps(state) {
  return {
    users: state.users,
    auth: state.auth,
  };
}

export default connect(mapStateToProps)(AllUsers);
