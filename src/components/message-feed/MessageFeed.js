import React, { Component, useEffect } from "react";
//, { useState }
import ProptTypes from "prop-types";
import { Loader } from "../loader";

export const MessageFeed = ({ getAllMessages, loading, error }) => {
  // Not to be confused with "this.setState" in classes\
  async function handleMessages(){
    let tempMessages = (await getAllMessages())
    tempMessages = {...tempMessages.messages}
    console.log(tempMessages)
    // let tempMessages = [...request.messages]
    //   return (tempMessages)
    return tempMessages
  }
  return (
    <React.Fragment>
      <p>{console.log(handleMessages())}</p>
      {loading && <Loader />}
      {error && <p style={{ color: "red" }}>{error.message}</p>}
    </React.Fragment>
  );
};

MessageFeed.propTypes = {
  loading: ProptTypes.bool,
  error: ProptTypes.string,
};
