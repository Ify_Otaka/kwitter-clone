import { createMuiTheme } from "@material-ui/core/styles";
// import purple from "@material-ui/core/colors/purple";
// import green from "@material-ui/core/colors/green";

const kwitterBlue = "#2AA3EF";
const kwitterGrey = "#F7F7F7";

export default createMuiTheme({
  palette: {
    common: {
      blue: `${kwitterBlue}`,
      grey: `${kwitterGrey}`,
    },
    primary: {
      main: `${kwitterBlue}`,
    },
    secondary: {
      main: `${kwitterGrey}`,
    },
  },
  typography: {
    tab:{
      fontFamily:"Raleway",
      textTransform: "none",
      fontWeight:700,
      fontSize:"1rem",
    }
  },
});
