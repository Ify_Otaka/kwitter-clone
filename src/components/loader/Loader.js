import React from "react";
// import { Button, Spinner } from 'react-bootstrap';
import {Button, CircularProgress } from '@material-ui/core';

/*
https://lmgtfy.com/?q=react+loading+spinner+components
There are many more on the web use one of these or create your own!
*/

export const Loader = () => 
<>
    
    <Button variant="primary" disabled>
        <CircularProgress
            as="span"
            animation="grow"
            size="sm"
            role="status"
            aria-hidden="true"
        />
        Loading...
    </Button>
    
</>;

