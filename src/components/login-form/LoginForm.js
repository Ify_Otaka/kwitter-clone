import React, { useState } from "react";
import ProptTypes from "prop-types";
import { Loader } from "../loader";
import { Link } from "react-router-dom";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
// import Link from '@material-ui/core/Link';
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
// import "./LoginForm.css";
import Copyright from "../copyright/Copyright";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(10),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(6),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export const LoginForm = ({ login, loading, error }) => {
  // Not to be confused with "this.setState" in classes
  const [state, setState] = useState({
    username: "",
    password: "",
  });

  const handleLogin = (event) => {
    event.preventDefault();

    login(state);
  };

  const handleChange = (event) => {
    const inputName = event.target.name;
    const inputValue = event.target.value;
    setState((prevState) => ({ ...prevState, [inputName]: inputValue }));
  };
  const classes = useStyles();
  return (
    <React.Fragment>
      <Container component="main" maxWidth="s">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}></Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>

          <form className="login-form" color="primary" onSubmit={handleLogin}>
            {/*<label htmlFor="username">Username</label>
             */}
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="username"
              name="username"
              value={state.username}
              autoComplete="email"
              autoFocus
              onChange={handleChange}
            />

            {/*<label htmlFor="password">Password</label>*/}
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              value={state.password}
              name="password"
              label="Password"
              type="password"
              id="password"
              onChange={handleChange}
              autoComplete="current-password"
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            <Link
            to={`/profiles/${state.username}`}
            >
              Log IN
            </Link>
            {/* <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              disabled={loading}
              onChange = {handleLogin}
            >
              Log In
            </Button> */}

            <Grid container>
              <Grid item xs>
                <Link
                  style={{ textDecoration: "inherit", color: "inherit" }}
                  href="#"
                  variant="body2"
                >
                  Forgot password?
                </Link>
              </Grid>
              <Grid item>
                <Link
                  style={{ textDecoration: "inherit", color: "inherit" }}
                  to="/register"
                  variant="body2"
                ></Link>
                {loading && <Loader />}
                Dont Have an Account? -<Link to="/register">Register Here</Link>
                {loading && <Loader />}
                {error && <p style={{ color: "red" }}>{error.message}</p>}
              </Grid>
            </Grid>
          </form>
        </div>
        <Box mt={8}>
          <Copyright />
        </Box>
      </Container>
    </React.Fragment>
  );
};

LoginForm.propTypes = {
  login: ProptTypes.func.isRequired,
  loading: ProptTypes.bool,
  error: ProptTypes.string,
};
