import { enhancer } from "./RegisterForm.enhancer";
import { RegisterForm } from "./RegisterForm";

export const RegisterFormContainer = enhancer(RegisterForm);
