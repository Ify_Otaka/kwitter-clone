import React from "react"
import Typography from '@material-ui/core/Typography';
import { Link } from "react-router-dom"
function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
       <Link style={{textDecoration:'inherit', color:'inherit'} }  color="inherit" href="https://material-ui.com/">
        kwitter
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}
export default Copyright