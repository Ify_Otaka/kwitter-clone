import React, { useState } from "react";
import ProptTypes from "prop-types";
import { Link } from "react-router-dom";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import hb from "../assests/hb.png";

//import { Button, Menu } from '@material-ui/core/Button';
// import "./Menu.css";

const useStyles = makeStyles((theme) => ({
  toolbarMargin: {
    ...theme.mixins.toolbar,
    marginBottom: "3rem",
  },
  tabContainer: {
    marginLeft: "auto",
  },
  tab: {
    ...theme.typography.tab,

    minWidth: 10,
    marginLeft: "25px",
  },
}));
export const Menu = ({ isAuthenticated, logout, username }) => {
  console.log("username: ", username);
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const handleChange = (e, value) => {
    setValue(value);
  };

  return (
    <React.Fragment>
      <AppBar position="fixed" color="primary">
        <Toolbar disableGutters>
          <Typography variant="h4" color="secondary">
            <img alt="logo" src={hb} />
            <Link style={{ textDecoration: "inherit", color: "inherit" }} to="">
              Kwitter
            </Link>
          </Typography>
          {isAuthenticated && (
            <React.Fragment>
              {/*<Link to="/messagefeed">Message Feed</Link>
          <Link to="/allusers">All Users</Link>
          <Link to="/" onClick={logout}>
            Logout
      </Link>*/}
              <Tabs
                value={value}
                onChange={handleChange}
                className={classes.tabContainer}
                indicatorColor="secondary"
              >
                <Tab
                  className={classes.tab}
                  component={Link}
                  to="/"
                  label="Home"
                ></Tab>
                <Tab
                  className={classes.tab}
                  component={Link}
                  to={`/profiles/${username}`}
                  label="Profile"
                ></Tab>
                <Tab
                  className={classes.tab}
                  component={Link}
                  to="/"
                  onClick={logout}
                  label="Logout"
                ></Tab>
                {/* <Tab className={classes.tab} component={Link} to="/allusers" label="All Users">
          </Tab> */}
              </Tabs>
            </React.Fragment>
          )}
        </Toolbar>
      </AppBar>
      <div className={classes.toolbarMargin} />
    </React.Fragment>
  );
};

Menu.defaultProps = {
  isAuthenticated: false,
  logout: () => {},
};

Menu.propTypes = {
  isAuthenticated: ProptTypes.bool.isRequired,
  logout: ProptTypes.func.isRequired,
};
