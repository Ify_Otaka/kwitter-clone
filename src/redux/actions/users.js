export const GET_USERS = "GET_USERS";

export const GET_PROFILE = "GET_PROFILE";

export const GET_PHOTO = "GET_PHOTO";

export const SET_PHOTO = "SET_PHOTO";

export const UPDATE_PHOTO = "UPDATE_PHOTO";

export const getUsersAction = (users) => {
  return {
    type: GET_USERS,
    users,
  };
};

export const getProfileAction = (user) => {
  return {
    type: GET_PROFILE,
    user,
  };
};

export const getPhotoAction = (user) => {
  return {
    type: GET_PHOTO,
    user,
  };
};

export const setPhotoAction = (user) => {
  return {
    type: SET_PHOTO,
    user,
  };
};

export const updatePhotoAction = (user) => {
  return {
    type: UPDATE_PHOTO,
    user,
  };
};
