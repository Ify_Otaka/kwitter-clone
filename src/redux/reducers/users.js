import { GET_USERS } from "../actions/users";
import { INITIAL_STATE } from "../reducers/auth";
import { GET_PROFILE } from "../actions/users";
import { GET_PHOTO } from "../actions/users";
import { UPDATE_PHOTO } from "../actions/users";

export const usersReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_USERS:
      return {
        ...state,
        users: action.users,
      };
    case GET_PROFILE:
      return {
        ...state,
        profile: action.user,
      };
    case GET_PHOTO:
      return {
        ...state,
        photo: action.user,
      };
    case UPDATE_PHOTO:
      return {
        ...state,
        photo: action.user,
      };
    default:
      return state;
  }
};
