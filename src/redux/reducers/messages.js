// TODO: implement
// import { GET_KWEETFEED } from "../actions/messages";
// import { INITIAL_STATE } from "../reducers/auth";

export const usersReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_KWEETFEED:
      return {
        ...state,
        messages: action.messages,
      };
    case GET_KWEET:
      return {
        ...state,
        profile: action.user,
      };
    default:
      return state;
  }
};
